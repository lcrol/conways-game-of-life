Conway's game of life simulation:

Rules:
    Any live cell with two or three live neighbours survives.
    Any dead cell with three live neighbours becomes a live cell.
    All other live cells die in the next generation. Similarly, all other dead cells stay dead.

To run:
    clone this project
    download most current version of node.js
    use npm to: npm install --global http-server
    cd to directory where cloned and run: http-server

Link to it running: https://lcrol.gitlab.io/conways-game-of-life/
