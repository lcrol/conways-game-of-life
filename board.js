class Board {
  constructor(cellWidth, height, width) {
    this.cellWidth = cellWidth;
    this.height = height;
    this.width = width;
    this.rows = Math.floor(this.width / this.cellWidth);
    this.cols = Math.floor(this.height / this.cellWidth);
    this.grid = new Array(this.rows);
    for (let i = 0; i < this.grid.length; i++) {
      this.grid[i] = new Array(this.cols);
    }
    for (let i = 0; i < this.grid.length; i++) {
      for (let j = 0; j < this.grid[i].length; j++) {
        let chance = Math.random();
        this.grid[i][j] = chance >= 0.5 ? 0 : 1;
      }
    }
  }

  drawCells = () => {
    for (let i = 0; i < this.grid.length; i++) {
      for (let j = 0; j < this.grid[i].length; j++) {
        if (this.grid[i][j] == 0) continue;
        fill(217);
        rect(
          i * this.cellWidth,
          j * this.cellWidth,
          this.cellWidth,
          this.cellWidth
        );
      }
    }
  };

  update = () => {
    this.drawCells();

    var next = new Array(this.rows);
    for (let i = 0; i < next.length; i++) {
      next[i] = new Array(this.cols);
    }

    for (let i = 0; i < this.grid.length; i++) {
      for (let j = 0; j < this.grid[i].length; j++) {
        let neighbors = this.countNeighbors(i, j);
        let cell = this.grid[i][j];

        if (cell === 0 && neighbors === 3) {
          next[i][j] = 1;
        } else if (cell === 1 && (neighbors < 2 || neighbors > 3)) {
          next[i][j] = 0;
        } else {
          next[i][j] = cell;
        }
      }
    }
    this.grid = next;
  };

  countNeighbors = (x, y) => {
    let sum = 0;
    for (let i = -1; i < 2; i++) {
      for (let j = -1; j < 2; j++) {
        let col = (x + i + this.cols) % this.cols;
        let row = (y + j + this.rows) % this.rows;
        sum += this.grid[col][row];
      }
    }
    sum -= this.grid[x][y];
    return sum;
  };
}
